<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function daftar()
    {
        return view('page.daftar');
    }


    public function kirim(Request $request)
    {

        $getnamadepan = $request->input('namadepan');
        $getnamabelakang = $request->input('namabelakang');
        // dd($getnamabelakang);

        // $data = [
        //     'namadepan' => $getnamadepan,
        //     'namabelakang' => $getnamabelakang,
        // ];

        // dd($data);
        return view('page.berhasil', ["namadepan" => $getnamadepan, "namabelakang" => $getnamabelakang]);
    }
}
